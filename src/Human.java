
public class Human extends Animal implements RockTools, BelajarCoding{

	String name;

	public Human(int limbs, String name) {
		super(limbs); //super untk memanggil constructor parent
		this.name = name;
	}

	@Override
	void cry() {
		System.out.println("human cry: oeee");
	}

	
	@Override
	void see() {
		System.out.println("human see: full color");
	}

	@Override
	public void smash() {
		System.out.println("manusia bisa smash dengn rocktools dengan iq minimal: "+RockTools.requiredIQ);
		
	}

	@Override
	public void belajarJava() {
		System.out.println("manusia bs belajar java");
	}
}
