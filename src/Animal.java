
public abstract class Animal {

	int limbs;

	public Animal(int limbs) {
		super();
		this.limbs = limbs;
	}
	
	abstract void cry(); //abtract method, kontrak mengikat harus di define oleh sub class
	
	abstract void see();
	
	void breath() { //bukan abstractmethod, bikin fungsi biasa masih bisa
		System.out.println("inhale...exhale");
		if(true) {
			System.out.println();
		}else {} //iterasi, sleksi bisa2 aja
	}
	
}
