
public class Cat extends Animal {

	int whiskers;

	public Cat(int limbs, int whiskers) {
		super(limbs);
		this.whiskers = whiskers;
	}

	@Override
	void cry() {
		System.out.println("cat cry: meow");
		
	}

	@Override
	void see() {
		System.out.println("cat see: color blind");
	}
	
	
}
